const fs = require('fs');

function readFile(file, options) {
  return new Promise((resolve, reject) => {
    fs.readFile(file, options, function (err, val) {
      if (err) {
        return reject(err);
      }

      return resolve(val);
    });
  });
}

function writeFile(file, data, options) {
  return new Promise((resolve, reject) => {
    fs.writeFile(file, data, options, function (err, val) {
      if (err) {
        return reject(err);
      }

      return resolve(val);
    });
  });
}

module.exports = {
  readFile,
  writeFile
};
