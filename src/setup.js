const jsyaml = require('js-yaml');
const glob = require('glob');
const readFile = require('./utils/file.utils').readFile;
const writeFile = require('./utils/file.utils').writeFile;

const OUTPUT_PATH = `./`;
const INCLUDE_SERVICES = {
  esdata: true,
  pgdata: true,
  zookeeper: true,
  kafka: true,
  redis: true,
};

const filePaths = glob.sync("./**/*+(.service|.base).yml");

Promise
  .all(filePaths.map(path => readFile(path, 'utf8')))
  .then(results => results.map(current => jsyaml.safeLoad(current)))
  .then(results => results.reduce((acc, val) => {
    if (val.service) {
      const serviceName = Object.keys(val.service)[0];
      if (INCLUDE_SERVICES[serviceName]) {
        acc.services = { ...acc.services, ...val.service };
      }
      return acc;
    } else {
      return { ...acc, ...val };
    }
  }, { services: {} }))
  .then(json => jsyaml.safeDump(json, { nonull: true }))
  .then(ymlstr => writeFile(`${OUTPUT_PATH}/docker-compose.yml`, ymlstr, 'utf8'))
  .then(console.log)
  .catch(console.error);
